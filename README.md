## Descripcion
Se realizo el proyecto como un test para Usuarios, el proyecto fue realizado en Laravel 10,
en una maquina Linux Ubuntu, con php 8, asegurese que tiene esta version de php.

## Crud para Usuarios con categorias y paises
En el test hay tres CRUDs una para usuarios, uno para Categorias de Usuario y uno de paises.

## Consideraciones

- Se realizo el proyecto con Laravle 10

- Se realizo la BD en Mysql 12

- Se reslizo un ccript de Pyhton en la carpeta Scripts que consume el API-Rest de paises y la inserta en la BD

- Se deben cambiar los parametros de conexion a Base de Datos del erchivo .env para conectarse a la   base de Datos.
 
- Una vez descargado el proyecto se deben generar la BD que debe llamarse `usuarios_gml`, en la direccion: `scripts/crear_data_base.sql` puede crear la base de datos.
 
- Creada la base de datos, puede generar las tablas a traves de la orden `php artisan migrate`, ya los archivos de migracion estan creados en la carpeta `database/migrations`.  ç

- Para crear el primer usuario debe ejecutar `php artisan migrate:refresh --seed`, esto recreara las tablas y los registros iniciales.
 
- Sí todo hasta este punto ha salido bien y no da errores ejecute: `php artisan serve --port=5000` y acceda por la direccion `"localhost:5000"`, el numero del puerto se puede setear a cualquierea que desee.
 
- Debe poder entrar al formulario login y usando los siguientes datos: `usuario:admin@bizion.com` y password:`123456` debe loguearse.



## Notas

Nombre: Domingo Utrera
ppt: 5311152 
Nacionalidad Venezolano
tel: +573134676290
Manizales, Caldas, Colombia
03 de Septiembre de 2023
correo: domingo140372@gmail.com

Cualquier pregunta o sugerencia comunicarse con el Autor
