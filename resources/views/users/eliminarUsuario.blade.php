<!-- Modal sencillo para eliminar usuario-->
<div class="modal fade" tabindex="-1" role="dialog" id="borrarModal{{$usuario->id}}">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Eliminar  Usuario</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">                   
                <form method="post" action="{{route('users.destroy', $usuario->id)}}">
                    @csrf
                    @method('DELETE')                    
                    <label for="correUsuario"><h5><strong>Desea eliminar el usuario: {{$usuario->name}} ??</strong></h5></label>
                    <div class="form-group">
                        <input type="hidden" id="idUsuario" name="idUsuario" value="{{$usuario->id}}">
                        <button type="submit" id ="btnBorrarUsuario" class="btn btn-danger">Eliminar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
                
            </div>
        
    </div>
</div>