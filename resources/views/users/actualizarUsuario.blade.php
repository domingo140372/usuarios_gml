
<!--
MOdal para Actualizar los Usuarios
Se envia el id desde la vista Index
-->

<div class="modal fade" tabindex="-1" role="dialog" id="actualModal{{$usuario->id}}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Actualizar Usuario</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form method="post" action="{{route('users.update', $usuario->id)}}">
                @csrf
                @method('PUT')
                <input type="hidden" id="idUsuario" name="idUsuario" value="{{$usuario->id}}">
                <div class="form-group">
                    <label for="nombreUsuario">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $usuario->name }}" aria-describedby="nombreUsuario" placeholder="Nombre de Usuario">
                    <small id="nombreUsuario" class="form-text text-muted">Nombre del usuario.</small>
                </div>
                <div class="form-group">
                    <label for="correUsuario">Correo electronico</label>
                    <input type="email" class="form-control" id="correo" name="correo" value="{{ $usuario->email }}" placeholder="Correo del usuario">
                    <small id="emailHelp" class="form-text text-muted">Correo del usuario.</small>
                </div>
                <div class="form-group">
                    <label for="idRol">Perfil</label>
                    <select class="form-select form-select-lg" id="idRol" name="idRol">
                            <option value="0" selected>Selecione un Rol</option>
                        @foreach($roles as $rol)
                            <option value="{{$rol->id}}">{{$rol->name}}</option>
                        @endforeach                 
                        
                    </select>                    
                </div>
                <div class="form-group">
                    <button type="submit" id ="btnActualUsuario" class="btn btn-info">Actualizar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
        
        </div>
    </div>
</div>