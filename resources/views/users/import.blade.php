<div id="importModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Contenido del modal -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h3><strong>
                        <p>Importar listado de usuarios</p>
                    </strong></h3>
                <form  method="post" action="{{route('users.import')}}" name="importForm" id="importtForm"  enctype="multipart/form-data" >
                    @csrf
                    @method('POST')
                    <div class="form-group">
                        <label for="file">Selecciona un archivo Excel</label>
                        <input type="file" name="file" id="file" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Importar</button>
                </form>
            </div>
        </div>
    </div>
</div>