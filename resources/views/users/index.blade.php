@extends('adminlte::page')

@section('title', 'Usuarios')

@section('content_header')
    <h1 class="m-0 text-dark">Usuarios</h1>    

@section('content')
<!-- Main content -->

<section class="content">
    <div class="container-fluid">
      <div class="card">
        <div class="card-header">
          <h1 class="card-title"><i class="fas fa-users"></i> Usuarios</h1>
        </div>
        <div class="row">
          <div class="col-md-12 col-xs-12">     
            <div class="row">
              <div id="message">
              <p> {{ session('success') }}</p>  
              </div>              
            </div>
             <!-- Enlace para abrir el modal -->
              <br><br>
                <button type="button" class="btn btn-success btn-sg" data-toggle="modal" data-target="#usuarioModal">Agregar Usuario</button>
                <button type="button" class="btn btn-success btn-sg" style="background-color: white; color:black;" data-toggle="modal" data-target="#importModal">Importar listado</button>
              @if (session('success'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
              @endif
              @if (session('danger'))
                <div class="alert alert-danger">
                    {{ session('message') }}
                </div>
              @endif     
          </div>
      </div>
        
        <div class="card-body">
          <div class="col-12 table-responsive">
              <table class="table table-bordered user_datatable">
                  <thead>
                      <tr>
                          <th>ID</th>
                          <th>Nombre</th>
                          <th>Email</th>
                          <th>Rol de usuario</th>
                          <th>Acciones</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($usuarios as $usuario)
                        <tr>                        
                            <td>{{$usuario->id}}</td>
                            <td>{{$usuario->name}}</td>
                            <td>{{$usuario->email}}</td>
                            <td>
                              @if(!empty($usuario->getRoleNames()))
                              @foreach($usuario->getRoleNames() as $v)
                              <label class="badge badge-success">{{ $v }}</label>
                              @endforeach
                              @endif
                            </td>
                            <td>
                                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#actualModal{{$usuario->id}}">Actualizar</button>
                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#borrarModal{{$usuario->id}}"> Eliminar </button>
                            </td>                        
                        </tr>
                        <!-- aqui van los modales-->

                      @include('users/actualizarUsuario')
                      @include('users/eliminarUsuario')
                      @endforeach
                  </tbody>
              </table>
            </div>
          </div>
        </div>
      
        <!-- Modal Nuevo usuario -->
        @include('users/insertarUsuario')
        @include('users/import')
    </div>
      
</section>
  
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <script type="text/javascript"> 
    
  </script>
@stop
