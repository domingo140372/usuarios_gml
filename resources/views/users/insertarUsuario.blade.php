<div id="usuarioModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Contenido del modal -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <h3><strong><p>Agregar Nuevo Usuario</p></strong></h3>
                <form method="post" action="{{route('users.store')}}" name="insertForm" id="insertForm">
                  @csrf
                  @method('POST')
                  <div class="form-group">
                    <label for="nombreUsuario">Nombre</label>
                    <input type="text" class="form-control" id="nombreUsuario" name="nombre" aria-describedby="nombreHelp" placeholder="Nombre de Usuario">
                    <small id="nombreHelp" class="form-text text-muted">Nombre del usuario.</small>
                  </div>
                  <div class="form-group">
                    <label for="correUsuario">Correo electronico</label>
                    <input type="email" class="form-control" id="correUsuario" name="correo" placeholder="Correo del usuario">
                    <small id="emailHelp" class="form-text text-muted">Correo del usuario.</small>
                  </div>
                  <div class="form-group">
                    <label for="claveUsuario">Nueva Clave</label>
                    <input type="password" class="form-control" id="claveUsuario" name="clave" placeholder="Clave del usuario">
                    <small id="claveHelp" class="form-text text-muted">Clave del usuario.</small>
                  </div>
                  <div class="form-group">
                    
                    <label for="idRol">Perfil</label>
                      <select class="form-select form-select-lg" id="idRol" name="idRol">
                          <option selected>Selecione un Rol</option>
                          @foreach($roles as $rol)
                          <option value="{{ $rol->id }}">{{ $rol->name }}</option>
                          @endforeach                      
                      </select>
                  </div>     
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary" id="insertarModal" >Ingresar Usuario</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
                  </div>
                </form>
            </div>
            
        </div>
    </div>
  </div>