<div class="modal fade" tabindex="-1" role="dialog" id="actualArticuloModal{{$articulo->id}}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Actualizar Articulo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('articulos.update', $articulo->id)}}">
                    @csrf
                    @method('PUT')
                    <input type="hidden" id="idArticulo" name="idArticulo" value="{{ $articulo->id }}">
                    <div class="form-group">
                        <label for="nombre">Articulo</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $articulo->nombre_articulo }}" aria-describedby="nombre" placeholder="Articulo">
                        <small id="nombre" class="form-text text-muted">Articulo.</small>
                    </div>
                    <div class="form-group">
                        <label for="codigo">Codigo</label>
                        <input type="text" class="form-control" id="codigo" name="codigo" value="{{ $articulo->codigo }}" aria-describedby="codigo" placeholder="Codigo">
                        <small id="codigo" class="form-text text-muted">Codigo.</small>

                        <label for="idRenglon">Renglon</label>
                        <select class="form-select form-select-lg" id="idRenglon" name="idRenglon">
                            <option selected value="{{ $articulo->renglones->id }}">{{ $articulo->renglones->nombre }}</option>
                            @foreach($renglones as $renglon)
                            <option value="{{ $renglon->id }}">{{ $renglon->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" id="btnActualArticulo" class="btn btn-info">Actualizar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>