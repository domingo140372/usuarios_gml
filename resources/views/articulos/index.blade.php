@extends('adminlte::page')

@section('title', 'Articulos')

@section('content_header')
<h1 class="m-0 text-dark">Articulos</h1>

@section('content')
<!-- Main content -->

<section class="content">
  <div class="container-fluid">

    <div class="card">
      <div class="card-header">
        <h1 class="card-title"><i class="fas fa-tag"></i>Articulos</h1>
      </div>
      <div class="row">
        <div class="col-md-12 col-xs-12">

          <!-- Enlace para abrir el modal -->
          <br><br>
          <div class="form-group">
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#addArticuloModal">Nuevo</button>
          </div>

          <br><br>
          @if (session('success'))
          <div class="alert alert-success">
            {{ session('message') }}
          </div>
          @endif
        </div>
      </div><!--  final de la fila del encabezado-->

      <div class="card-body">
        <div class="col-12 table-responsive">
        <table class="table table-bordered articul_datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Articulo</th>
                        <th>Renglon</th>
                        <th>Codigo</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($articulos as $articulo)
                    <tr>
                        <td>{{$articulo->id}}</td>
                        <td>{{$articulo->nombre_articulo}}</td>
                        <td>{{$articulo->renglones->nombre}}</td>
                        <td>{{$articulo->codigo}}</td>
                        <td>
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#actualArticuloModal{{$articulo->id}}">Actualizar</button>
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#borrarArticuloModal{{$articulo->id}}"> Eliminar </button>
                        </td>
                    </tr>
                    <!--MOdales para actualizar y Borrar-->
                    @include('articulos/actualizarArticulo')
                    @include('articulos/eliminarArticulo')
                    @endforeach
                </tbody>
          </table>

        </div>
      </div>
    </div><!--  final de card principal-->

    <!-- Modal Nuevo usuario -->
    @include('articulos/insertArticulo')


  </div>
</section>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script type="text/javascript">

</script>
@stop