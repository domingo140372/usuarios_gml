<!-- Modal sencillo para eliminar usuario-->
<div class="modal fade" tabindex="-1" role="dialog" id="borrarArticuloModal{{$articulo->id}}">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Eliminar Articulo</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">                   
                <form method="post" action="{{route('articulos.destroy', $articulo->id)}}">
                    @csrf
                    @method('DELETE')                    
                    <label for="articulo"><h5><strong>Desea eliminar el articulo: {{ $articulo->nombre_articulo }} ??</strong></h5></label>
                    <div class="form-group">
                        <input type="hidden" id="idarticulo" name="idarticulo" value="{{$articulo->id}}">
                        <button type="submit" id ="btnBorrarArticulo" class="btn btn-danger">Eliminar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
                
            </div>
        
    </div>
</div>