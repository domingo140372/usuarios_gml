<div class="modal fade" tabindex="-1" role="dialog" id="addArticuloModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Nuevo Articulo</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form method="post" action="{{route('articulos.store')}}">
                @csrf
                @method('POST')                
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre"  aria-describedby="nombre" placeholder="Nombre">
                    <small id="nombre" class="form-text text-muted">Nombre.</small>
                </div> 
                <div class="form-group">
                    <label for="codigo">Codigo</label>
                    <input type="text" class="form-control" id="codigo" name="codigo"  aria-describedby="codigo" placeholder="Codigo">
                    <small id="codigo" class="form-text text-muted">Codigo.</small>
                </div>
                <label for="idRenglon">Renglon</label>
                      <select class="form-select form-select-lg" id="idRenglon" name="idRenglon">
                          <option selected>Selecione un Renglon</option>
                          @foreach($renglones as $renglon)
                          <option value="{{ $renglon->id }}">{{ $renglon->nombre }}</option>
                          @endforeach                      
                      </select>
                <div class="form-group">
                    <button type="submit" id ="btnInsertarArticulo" class="btn btn-info">Guardar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>        
        </div>
    </div>
</div>