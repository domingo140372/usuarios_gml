@extends('adminlte::page')

@section('title', 'Articulos Genericos')

@section('content_header')
<h1 class="m-0 text-dark">Actualizacion  de Genericos</h1>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<link id="bs-css" href="https://netdna.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
<link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h1 class="card-title"><i class="fas fa-motorcycle"></i>Ingreso de Genericos</h1>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hay un problema con el registro<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <!--  -->
                    <br><br>
                    <div class="col">
                        <a class="btn btn-primary" href="{{ route('genericos.index') }}"> Volver</a>
                    </div>
                    <br><br>
                </div>
            </div>
            <div class="card-body">
                <!-- aqui va el formulario de actualizacion-->
                <form method="post" action="{{route('genericos.update',$generico->id)}}" name="insertGenerico" id="insertGenerico">
                    @csrf
                    @method('PUT')
                    <div class="form-row">
                    <input type="hidden" id="idGenrico" name="id_generico" value="{{$generico->id}}">
                        <div class="form-group col-md-4">
                            <label for="idArticulo">Articulo</label>
                            <select class="form-select form-select-lg" id="id_articulo" name="id_articulo">
                            <option selected value="{{$generico->articulos->id}}">{{$generico->articulos->nombre_articulo }}</option>
                                @foreach($articulos as $articulo)
                                <option value="{{ $articulo->id }}">{{ $articulo->nombre_articulo }}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="idUnidad">Unidad de Medida</label>
                            <select class="form-select form-select-lg" id="id_unidad" name="id_unidad">
                                <option selected value="{{$generico->unidades->id}}">{{ $generico->unidades->unidad }}</option>
                                @foreach($unidades as $unidad)
                                <option value="{{ $unidad->id }}">{{ $unidad->unidad }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputModelo">Modelo</label>
                            <input type="text" class="form-control" name="modelo" id="modelo" value="{{$generico->modelo}}">
                            
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputMarca">Marca</label>
                            <input type="text" class="form-control" name="marca" id="marca" value="{{ $generico->marca }}l Articulo">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="cantidad">Cantidad</label>
                            <input type="text" class="form-control" name="cantidad" id="cantidad" value="{{ $generico->cantidad }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="descripcion">Descripción</label>
                            <input type="text" class="form-control" name="descripcion" id="descripcion" value="{{ $generico->descripcion }}ion">
                        </div>

                    </div>
                    <div class="form-row">

                        <div class='col-sm-4'>
                            <div class="form-group">
                                <label for="fecha_ingreso">Fecha de ingreso:</label>
                                <input type='date' class="form-control" id="fecha_ingreso" name="fecha_ingreso" />
                            </div>
                        </div>
                        <div class='col-sm-4'>
                            <div class="form-group">
                                <label for="fecha_salida">Fecha de salida:</label>
                                <input type='date' class="form-control" id="fecha_salida" name="fecha_salida" />
                            </div>
                        </div>


                        <div class="form-group col-md-4">
                            <label for="codigo">Codigo</label>
                            <input type="text" class="form-control" id="codigo" name="codigo" value="{{ $generico->codigo }}e Almacen ">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="observacion">Observaciones</label>
                            <textarea class="form-control" name="observacion" id="observacion" rows="3">{{$generico->observacion}}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="estado">Estado</label>
                            <input type="text" class="form-control" id="estado" name="estado" value="{{ $generico->estado }}">
                        </div>
                        <div class="form-group col-sm-2">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="form-check">
                                        <input style="margin-left:5px;" type="checkbox" value="{{ $generico->entregada}}" id="entregada" name="entregada">
                                        <label class="form-check-label" for="entregada">Entregada</label>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>

                </form>
            </div>
        </div>

</section>
<p class="text-center text-primary"><small>actualizar de Articulo Generico</small></p>

@endsection

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>


<script type="text/javascript">

</script>
@stop