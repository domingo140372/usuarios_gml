@extends('adminlte::page')

@section('title', 'Ingreso de motos')

@section('content_header')
<h1 class="m-0 text-dark">Gennericos</h1>

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h3>Inventario de Genericos</h3>
        </div>
        <div class="row w-100 text-right" style="margin-top:10px; margin-right:50px">
            <div class="col">
                @can('product-create')
                <a class="btn btn-success" href="{{ route('genericos.create') }}">Nuevo registro</a>
                @endcan
                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
              @endif  
            </div>
        </div>
    </div>
</div>
<div class="card-body">
    <div class="col-12 table-responsive">
        <table class="table table-bordered moto_datatable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Articulo</th>
                    <th>Fecha de Salida</th>
                    <th>Cantidad</th>
                    <th>Unidad med.</th>
                    <th>Descripcion</th>
                    <th>Observación</th>
                    <th>Fotos</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($genericos as $generico)
                <tr>
                    <td>{{$generico->id}}</td>
                    <td>{{$generico->articulos->nombre_articulo}}</td>
                    <td>{{$generico->fecha_salida}}</td>
                    <td>{{$generico->cantidad}}</td>
                    <td>{{$generico->unidades->unidad}}</td>
                    <td>{{$generico->descripcion}}</td>
                    <td>{{$generico->observacion}}</td>
                    <td>    
                        <button type="button" id="fotosGen" class="btn btn-info btn-sm" data-toggle="modal" data-target="#fotoModal{{$generico->id}}">fotos</button>
                    </td>
                    <td>
                        <a class="btn btn-info btn-sm" href="{{ route('genericos.edit', $generico->id) }}">actualizar</a>
                        <button type="button" id="eliminarGen" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#eliminarGenModal{{$generico->id}}"> Eliminar </button>
                    </td>
                    
                </tr>                
                @include('genericos/insertarFotos')
                @include('genericos/eliminar')
                @endforeach
            </tbody>
        </table>
    </div>

</div>
</div>

@endsection

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script type="text/javascript">

</script>
@stop