<!-- Modal sencillo para eliminar Pais-->
<div class="modal fade" tabindex="-1" role="dialog" id="eliminarGenModal{{$generico->id}}">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Eliminar Articulo</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">                   
                <form method="post" action="{{route('genericos.borrado', $generico->id)}}">
                    @csrf
                    @method('POST')                    
                    <label for="generico"><h5><strong>Desea eliminar este Articulo: {{$generico->modelo}} ??</strong></h5></label>
                    <div class="form-group">
                        <input type="hidden" id="id_generico" name="id_generico" value="{{$generico->id}}">
                        <button type="submit" id ="btnBorrarGenerico" class="btn btn-danger">Eliminar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
                
            </div>
        
    </div>
</div>