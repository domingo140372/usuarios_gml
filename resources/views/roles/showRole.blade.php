<div id="RolesShowModal{{$role->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Contenido del modal -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <h3><strong><p>Listado de roles</p></strong></h3>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                        <strong>Name:</strong>
                        {{ $role->name }}
                    </div>
                </div>
                
                <form method="post" action="{{ route('roles.show',$role->id) }}">
                @csrf
                @method('POST')
                <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Permissions:</strong>
                        </div>
                    </div>
                </form>
                
            </div><!-- Modal body-->
        </div><!--Modal content-->
    </div><!--Modal dialog-->
</div><!--Form Modal-->