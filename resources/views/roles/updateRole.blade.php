<div class="modal fade" tabindex="-1" role="dialog" id="updateRoleModal{{$role->id}}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Actualizar Role</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Permission:</strong>
                            <br/>
                            @php
                                //var_dump($permissions[4]->id);
                               
                                $rolPermissions = array('rolpermissions'=>$role->permissions,);
                                $listaPermisos = array();
                                foreach($rolPermissions['rolpermissions'] as $permi=>$value){
                                    $listaPermisos[]=$value->id;
                                }
                               
                            @endphp
                            @foreach($permissions as $permission)
                                    
                                    <label>{{ Form::checkbox('permission[]', $permission->id, in_array($permission->id, $listaPermisos, true) ? true : false, array('class' => 'name')) }}
                                    {{ $permission->name }}</label>
                                
                                <br/>                               
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Actualizar</button>
                {!! Form::close() !!}
            </div><!--body-->     
        </div>
    </div>
</div>