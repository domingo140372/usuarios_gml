<div id="RolesInsertModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Contenido del modal -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <h3><strong><p>Agregar Nuevo Rol</p></strong></h3>
              {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
                <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Name:</strong>
                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Permission:</strong>
                <br/>
                @foreach($permissions as $value)
                <label>{{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name')) }}
                {{ $value->name }}</label>
                <br/>
                @endforeach
                </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div><!--content-->
    </div><!--diañog-->
</div><!--Modal-->