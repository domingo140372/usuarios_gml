@extends('adminlte::page')

@section('title', 'Roles')

@section('content_header')
    <h1 class="m-0 text-dark">  Roles y permisos</h1>    

@section('content')
    <div class="card">
        <div class="card-header">
          <h1 class="card-title"><i class="fas fa-users"></i> Roles y permisos</h1>
        </div>
        <div class="row">
          <div class="col-md-12 col-xs-12">     
            
             <!-- Enlace para abrir el modal -->
              <br><br>
                <div class="pull-right">
                    @can('role-create')
                        <a class="btn btn-success"  data-toggle="modal" data-target="#RolesInsertModal">Nuevo Rol</a>
                    @endcan
                </div>
                
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
          </div>
      </div>
      <br>

        
    <div class="col-12 table-responsive">
        <table class="table table-bordered">
            <tr>
                <th>Nro</th>
                <th>Name</th>
                <th width="280px">Action</th>
            </tr>
            @foreach ($roles as $key => $role)            
            <tr>
                <td>{{ $role->id }}</td>
                <td>{{ $role->name }}</td>
                <td>                
                @can('role-edit')
                <a class="btn btn-primary" data-toggle="modal"  data-target="#updateRoleModal{{ $role->id }}" >Editar</a>
                @endcan
                @can('role-delete')
                {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy',
                $role->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
                @endcan
                </td>
            </tr>            
            @include('roles/updateRole')
            @endforeach
        </table>
    </div>
        

      {!! $roles->render() !!}
     <p class="text-center text-primary"><small>lista de roles de Usuarios</small></p>

     @include('roles/insertRole')
@endsection
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script type="text/javascript">

</script>
@stop