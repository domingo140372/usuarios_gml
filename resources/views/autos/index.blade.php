@extends('adminlte::page')

@section('title', 'Ingreso de autos')

@section('content_header')
<h1 class="m-0 text-dark">Automoviles</h1>

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h3>Inventario de Autos y Camiones</h3>
        </div>
        <div class="row w-100 text-right" style="margin-top:10px; margin-right:50px">
            <div class="col">
                @can('product-create')
                <a class="btn btn-success" href="{{ route('autos.create') }}">Nuevo registro</a>
                @endcan
            </div>
        </div>
    </div>
</div>
<div class="card-body">
    <div class="col-12 table-responsive">
        <table class="table table-bordered moto_datatable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Modelo</th>
                    <th>Articulo</th>
                    <th>Fecha de Salida</th>
                    <th>Placa</th>
                    <th>Observación</th>
                    <th>Fotos</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($autos as $auto)
                <tr>
                    <td>{{$auto->id}}</td>
                    <td>{{$auto->modelo}}</td>
                    <td>{{$auto->articulos->nombre_articulo}}</td>
                    <td>{{$auto->fecha_salida}}</td>
                    <td>{{$auto->placa}}</td>
                    <td>{{$auto->observacion}}</td>
                    <td>
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#fotoModal{{$auto->id}}">fotos</button>
                    </td>
                    <td>
                        <a class="btn btn-info btn-sm" href="{{ route('autos.edit', $auto->id) }}">actualizar</a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#borrarModal{{$auto->id}}"> Eliminar </button>
                    </td>
                </tr>
                @include('autos/insertarFotos')
                @include('autos/eliminar')
                @endforeach
            </tbody>
        </table>
    </div>

</div>
</div>

@endsection

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script type="text/javascript">

</script>
@stop