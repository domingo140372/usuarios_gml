<!-- Modal sencillo para eliminar Pais-->
<div class="modal fade" tabindex="-1" role="dialog" id="eliminarModal{{$auto->id}}">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Eliminar vehiculo</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">                   
                <form method="post" action="{{route('autos.borrado', $auto->id)}}">
                    @csrf
                    @method('DELETE')                    
                    <label for="auto"><h5><strong>Desea eliminar este Vehiculo: {{$auto->modelo}} ??</strong></h5></label>
                    <div class="form-group">
                        <input type="hidden" id="id_auto" name="id_auto" value="{{$auto->id}}">
                        <button type="submit" id ="btnBorrarAuto" class="btn btn-danger">Eliminar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
                
            </div>
        
    </div>
</div>