<!-- Modal sencillo para eliminar usuario-->
<div class="modal fade" tabindex="-1" role="dialog" id="borrarRenglonModal{{$renglon->id}}">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Eliminar Reglon</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">                   
                <form method="post" action="{{route('renglones.destroy', $renglon->id)}}">
                    @csrf
                    @method('DELETE')                    
                    <label for="renglon"><h5><strong>Desea eliminar el renglon: {{ $renglon->nombre }} ??</strong></h5></label>
                    <div class="form-group">
                        <input type="hidden" id="idrenglon" name="idrenglon" value="{{$renglon->id}}">
                        <button type="submit" id ="btnBorrarRenglon" class="btn btn-danger">Eliminar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
                
            </div>
        
    </div>
</div>