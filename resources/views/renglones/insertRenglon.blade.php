<div class="modal fade" tabindex="-1" role="dialog" id="addRenglonModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Nuevo Renglon</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form method="post" action="{{route('renglones.store')}}">
                @csrf
                @method('POST')                
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre"  aria-describedby="nombre" placeholder="Nombre">
                    <small id="nombre" class="form-text text-muted">Nombre.</small>
                </div> 
                <div class="form-group">
                    <label for="descripcion">Descripcion</label>
                    <input type="text" class="form-control" id="descripcion" name="descripcion"  aria-describedby="descripcion" placeholder="Descripcion">
                    <small id="Renglon" class="form-text text-muted">Descripcion.</small>
                </div> 
                <div class="form-group">
                    <button type="submit" id ="btnInsertarRenglon" class="btn btn-info">Guardar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>        
        </div>
    </div>
</div>