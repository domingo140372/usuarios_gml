<div class="modal fade" tabindex="-1" role="dialog" id="actualRenglonModal{{$renglon->id}}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Actualizar Renglon</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form method="post" action="{{route('renglones.update', $renglon->id)}}">
                @csrf
                @method('PUT')
                <input type="hidden" id="idRenglon" name="idRenglon" value="{{ $renglon->id }}">
                <div class="form-group">
                    <label for="nombre">Renglon</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $renglon->nombre }}" aria-describedby="nombre" placeholder="Renglon">
                    <small id="nombre" class="form-text text-muted">Renglon.</small>
                </div> 
                <div class="form-group">
                    <label for="descripcion">Descripcion</label>
                    <input type="text" class="form-control" id="descripcion" name="descripcion" value="{{ $renglon->descripcion }}" aria-describedby="descripcion" placeholder="descripcion">
                    <small id="descripcion" class="form-text text-muted">Descripcion.</small>
                </div>                
                <div class="form-group">
                    <button type="submit" id ="btnActualRenglon" class="btn btn-info">Actualizar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
        
        </div>
    </div>
</div>