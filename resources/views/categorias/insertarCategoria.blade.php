<div class="modal fade" tabindex="-1" role="dialog" id="categoriaModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Nuevo Perfil</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form method="post" action="{{route('categorias.store')}}">
                @csrf
                @method('POST')                
                <div class="form-group">
                    <label for="categoria">Perfil</label>
                    <input type="text" class="form-control" id="categoria" name="categoria"  aria-describedby="categoria" placeholder="Nuevo Perfil">
                    <small id="categoria" class="form-text text-muted">Perfil.</small>
                </div> 
                <div class="form-group">
                    <button type="submit" id ="btnInsertarCategoria" class="btn btn-info">Guardar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>        
        </div>
    </div>
</div>