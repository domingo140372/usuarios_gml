@extends('adminlte::page')

@section('title', 'Categorias')

@section('content_header')
<h1 class="m-0 text-dark">Perfiles</h1>

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h1 class="card-title"><i class="fas fa-map"></i>Perfiles</h1>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div id="messages"></div>
                    <!-- Enlace para abrir el modal -->
                    <br><br>
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#categoriaModal">Agregar Perfil</button>
                    <br><br>
                </div>
            </div>
            <div class="card-body">
                <div class="col-12 table-responsive">
                    <table class="table table-bordered user_datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Perfil de Usuario</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categorias as $categoria)
                            <tr>
                                <td>{{$categoria->id}}</td>
                                <td>{{$categoria->categoria}}</td>
                                <td>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#actualModal{{$categoria->id}}">Actualizar</button>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#borrarModal{{$categoria->id}}"> Eliminar </button>
                                </td>
                            </tr>
                            @include('categorias/actualizarCategoria')
                            @include('categorias/eliminarCategoria')
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @include('categorias/insertarCategoria')
        </div>

</section>
@stop