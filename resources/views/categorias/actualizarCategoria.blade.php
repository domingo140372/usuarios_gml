<div class="modal fade" tabindex="-1" role="dialog" id="actualModal{{$categoria->id}}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Actualizar Perfil</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form method="post" action="{{route('categorias.update', $categoria->id)}}">
                @csrf
                @method('PUT')
                <input type="hidden" id="idCategoria" name="idCategoria" value="{{$categoria->id}}">
                <div class="form-group">
                    <label for="categoria">Perfil</label>
                    <input type="text" class="form-control" id="categoria" name="categoria" value="{{ $categoria->categoria }}" aria-describedby="categoria" placeholder="Categoria">
                    <small id="categoria" class="form-text text-muted">Perfil.</small>
                </div>                
                <div class="form-group">
                    <button type="submit" id ="btnActualCategoria" class="btn btn-info">Actualizar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
        
        </div>
    </div>
</div>