<!-- Modal sencillo para eliminar usuario-->
<div class="modal fade" tabindex="-1" role="dialog" id="borrarModal{{$categoria->id}}">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Eliminar Perfil</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">                   
                <form method="post" action="{{route('categorias.destroy', $categoria->id)}}">
                    @csrf
                    @method('DELETE')                    
                    <label for="categoria"><h5><strong>Desea eliminar perfil: {{ $categoria->categoria }} ??</strong></h5></label>
                    <div class="form-group">
                        <input type="hidden" id="idCategoria" name="idCategoria" value="{{$categoria->id}}">
                        <button type="submit" id ="btnBorrarCategoria" class="btn btn-danger">Eliminar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
                
            </div>
        
    </div>
</div>