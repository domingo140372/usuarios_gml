<!-- Modal sencillo para eliminar Pais-->
<div class="modal fade" tabindex="-1" role="dialog" id="eliminarFotoModal{{$foto->id}}">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Eliminar Foto</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">                   
                <form method="post" action="{{route('fotoMoto.destroy', $foto->id)}}">
                    @csrf
                    @method('DELETE')                    
                    <label for="foto"><h5><strong>Desea eliminar esta Moto: {{$foto->modelo}} ??</strong></h5></label>
                    <div class="form-group">
                        <input type="hidden" id="id_foto" name="id_foto" value="{{$foto->id}}">
                        <button type="submit" id ="btnBorrarpais" class="btn btn-danger">Eliminar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
                
            </div>
        
    </div>
</div>