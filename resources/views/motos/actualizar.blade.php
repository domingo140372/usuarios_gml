@extends('adminlte::page')

@section('title', 'Motos')

@section('content_header')
<h1 class="m-0 text-dark">Actualizar  Moto</h1>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<link id="bs-css" href="https://netdna.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
<link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h1 class="card-title"><i class="fas fa-motorcycle"></i>Ingreso de Motos</h1>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hay un problema con el registro<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <!--  -->
                    <br><br>
                        <div class="col">
                        <a class="btn btn-primary" href="{{ route('motos.index') }}"> Volver</a>
                        </div>
                    <br><br>
                </div>
            </div>
            <div class="card-body">
                <!-- aqui va el formulario de registro  de motos-->
                <form method="post" action="{{route('motos.update', $moto->id )}}" name="updateMotos" id="updateMotos">
                  @csrf
                  @method('PUT')
                    <div class="form-row">
                    <input type="hidden" id="idMoto" name="idMoto" value="{{ $moto->id }}">
                        <div class="form-group col-md-4">
                            <label for="inputModelo">Modelo</label>
                            <input type="text" class="form-control" name="modelo" id="modelo" value="{{ $moto->modelo }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputMarca">Marca</label>
                            <input type="text" class="form-control" name="marca" id="marca" value="{{ $moto->marca }}">
                        </div>
                        <div class="form-group col-md-4">
                        <label for="idArticulo">Articulo</label>
                        <select class="form-select form-select-lg" id="idArticulo" name="idArticulo">
                            <option selected value="{{$moto->articulos->id}}">{{$moto->articulos->nombre_articulo}}</option>
                            @foreach($articulos as $articulo)
                            <option value="{{ $articulo->id }}">{{ $articulo->nombre_articulo }}</option>
                            @endforeach
                        </select>

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputSerialChasis">Serial Chasis</label>
                            <input type="text" class="form-control" name="serial_chasis" id="inputSerialChasis" value="{{ $moto->serial_chasis }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputSerialMotor">Serial Motor</label>
                            <input type="text" class="form-control" name="serial_motor" id="inputSerialMotor" value="{{ $moto->serial_motor }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputCilindrada">Cilindrada</label>
                            <input type="text" class="form-control" name="cilindrada" id="inputCilindrada" value="{{ $moto->cilindrada }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inpuColor">Color:</label>
                            <input type="text" class="form-control" name="color" id="inpuColor" value="{{ $moto->color }}">
                        </div>
                        <div class='col-sm-4'>
                            <div class="form-group">
                                <label for="fechaSalida">Fecha de entrada:</label>
                                <input type='date' class="form-control" id="fecha_entrada" name="fecha_entrada" value="{{ $moto->fecha_entrada }}"/>
                            </div>
                        </div>
                        <div class='col-sm-4'>
                            <div class="form-group">
                                <label for="fechaSalida">Fecha de salida:</label>
                                <input type='date' class="form-control" id="fecha_salida" name="fecha_salida"  value="{{ $moto->fecha_salida }}"/>
                            </div>
                        </div>

                        
                    </div>


                    <div class="form-row">
                    <div class="form-group col-md-4">
                            <label for="inputPlaca">Placa</label>
                            <input type="text" class="form-control" id="inputPlaca" name="placa" value="{{ $moto->placa }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputFacturacion">Facturación:</label>
                            <input type="text" class="form-control" name="facturacion" id="inputFacturacion" value="{{ $moto->facturacion }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputDestinatario">Destinatario:</label>
                            <input type="text" class="form-control" name="destinatario" id="inputDestinatario" value="{{ $moto->destinatario }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="Observaciones">Observaciones</label>
                            <textarea class="form-control" name="observacion" id="Observaciones" rows="3" >{{ $moto->observacion }}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-2">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="form-check">
                                        <input style="margin-left:5px;" type="checkbox" value="" id="rotulada" name="roltulada">
                                        <label class="form-check-label" for="rotulada">Rotulada</label>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="form-check">
                                        <input style="margin-left:5px;" type="checkbox" value="" id="operativa" name="operativa">
                                        <label class="form-check-label" for="operativa">Operativa</label>
                                    </div>

                                </li>
                                <li class="list-group-item">
                                    <div class="form-check">
                                        <input style="margin-left:5px;" type="checkbox" value="" id="entregada" name="entregada">
                                        <label class="form-check-label" for="operativa">Entregada</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>

                </form>
            </div>
        </div>

</section>
<p class="text-center text-primary"><small>Actulizar de Motos</small></p>

@endsection

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>


<script type="text/javascript">

</script>
@stop