<div id="fotoModal{{$moto->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Contenido del modal -->
        <div class="modal-content">
            <div class="modal-header">
                <h4><strong>
                        <p>Agregar Fotos</p>
                    </strong></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-6">
                        <strong>
                            {{$moto->marca}}&nbsp;|
                            {{$moto->modelo}}&nbsp;|
                            {{$moto->placa}}
                        </strong>
                        <br>
                        <strong>Observaciones: </strong> {{$moto->observacion}}
                    </div>
                    <div class="col-sm-6">
                        <form action="{{route('fotoMoto.store', $moto->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('POST')
                            <div class="form-group">
                                <div class="file is-info has-name is-boxed">
                                    <label class="file-label">
                                        @csrf
                                        <input type="hidden" name="id_moto" value="{{$moto->id}}">
                                        <div>
                                            <label for="images">Seleccione las fotos:</label>
                                            <input type="file" name="fotos[]" id="fotos" multiple required>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="field"><br>
                                <button :disabled="fotos.length <= 0" class="button is-success" type="submit">Subir</button>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12 col-sm-12">
                        <h5 style="text-align: center;">Fotos</h5>
                    </div>
                </div>
                <hr>
                <div class="row">
                
                    @php
                        $mostrarPorFila = 3
                    @endphp
                    @forelse($moto->fotosMoto as $foto)
        
                    {{--
                        Nota: los comentarios suponen que $mostrarPorFila es 3. Si no, igual funciona pero no entenderás los comentarios
                        Abrir div si es elemento es 1, 4, 7, etcétera (comenzando a contar en 1)
                    --}}
                    @if($loop->iteration % $mostrarPorFila === 1)
                    <div class="row">
                        @endif
                        <div class="col-8 col-sm-6">
                            <div class="card">
                                <div class="card-image">
                                    <figure class="image is-4by3">
                                        <img src="{{route("fotoMoto.get", ["nombre" => $foto->ruta])}}" alt="Placeholder image">
                                    </figure>
                                </div>
                                <footer class="card-footer">
                                    <a href="{{route("fotoMoto.get", ["nombre" => $foto->ruta])}}" target="_blank" class="card-footer-item">Ampliar</a>                                    

                                </footer>
                            </div>
                        </div>
                        {{--
                        Cerrar el div. Ya sea porque llegamos al final y corrimos con la suerte de que el
                        arreglo tuviera una longitud múltipla de 3 ($loop->iteration % 3 === 0 && $loop->iteration !== 0)

                         O cerrarlo porque es el último elemento y la longitud no era múltiplo de 3
                         ($loop->last && $loop->count % 3 !== 0)
                    --}}
                     
                     @if(($loop->iteration % $mostrarPorFila === 0 && $loop->iteration !== 0) || ($loop->last && $loop->count % $mostrarPorFila !== 0))
                    </div>
                    @endif
                    @empty
                    <div class="row">
                        <div class="col">
                            <h3 class="is-size-1">No hay fotos</h3>
                        </div>
                    </div>
                   
                    @endforelse
                </div>
            </div>
            </div><!-- modal body -->
        </div>
    </div>
</div>