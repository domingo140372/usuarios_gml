<!-- Modal sencillo para eliminar Pais-->
<div class="modal fade" tabindex="-1" role="dialog" id="eliminarModal{{$moto->id}}">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Eliminar Moto</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">                   
                <form method="post" action="{{route('motos.borrado', $moto->id)}}">
                    @csrf
                    @method('POST')                    
                    <label for="moto"><h5><strong>Desea eliminar esta Moto: {{$moto->modelo}} ??</strong></h5></label>
                    <div class="form-group">
                        <input type="hidden" id="id_moto" name="id_moto" value="{{$moto->id}}">
                        <button type="submit" id ="btnBorrarpais" class="btn btn-danger">Eliminar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
                
            </div>
        
    </div>
</div>