<!-- Modal sencillo para eliminar usuario-->
<div class="modal fade" tabindex="-1" role="dialog" id="borrarDefArticuloModal{{$defarticulo->id}}">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Eliminar Articulo</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">                   
                <form method="post" action="{{route('defarticulos.destroy', $defarticulo->id)}}">
                    @csrf
                    @method('DELETE')                    
                    <label for="articulo"><h5><strong>Desea eliminar el campo definido: {{ $defarticulo->definicion_campo }} ??</strong></h5></label>
                    <div class="form-group">
                        <input type="hidden" id="idarticulo" name="idarticulo" value="{{ $defarticulo->id }}">
                        <button type="submit" id ="btnBorrarDefArticulo" class="btn btn-danger">Eliminar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
                
            </div>
        
    </div>
</div>