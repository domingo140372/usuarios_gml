@extends('adminlte::page')

@section('title', 'Definiciones de Articulos')

@section('content_header')
<h1 class="m-0 text-dark">Definiciones de Articulos</h1>

@section('content')
<!-- Main content -->

<section class="content">
  <div class="container-fluid">

    <div class="card">
      <div class="card-header">
        <h1 class="card-title"><i class="fas fa-tag"></i>Definiciones de Articulos</h1>
      </div>
      <div class="row">
        <div class="col-md-12 col-xs-12">

          <!-- Enlace para abrir el modal -->
          <br><br>
          <div class="form-group">
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#addDefArticuloModal">Nuevo</button>
          </div>

          <br><br>
          @if (session('success'))
          <div class="alert alert-success">
            {{ session('message') }}
          </div>
          @endif
        </div>
      </div><!--  final de la fila del encabezado-->

      <div class="card-body">
        <div class="col-12 table-responsive">
        <table class="table table-bordered arti_datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Articulo</th>
                        <th>Campo</th>
                        <th>Unidad de Medida</th>
                        <th>Nota</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($defarticulos as $defarticulo)
                    <tr>
                        <td>{{$defarticulo->id}}</td>
                        <td>{{$defarticulo->articulos?->nombre_articulo}}</td>
                        <td>{{$defarticulo->definicion_campo}}</td>                        
                        <td>{{$defarticulo->unidades->unidad}}</td>
                        <td>{{$defarticulo->nota_adicional}}</td>
                        <td>
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#actualDefArticuloModal{{$defarticulo->id}}">Actualizar</button>
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#borrarDefArticuloModal{{$defarticulo->id}}"> Eliminar </button>
                        </td>
                    </tr>
                    <!--MOdales para actualizar y Borrar -->
                    @include('defarticulos/actualizarDefArticulo')
                    @include('defarticulos/eliminarDefArticulo') 
                    @endforeach
                </tbody>
          </table>

        </div>
      </div>
    </div><!--  final de card principal-->

    <!-- Modal Nueva definicion -->
    @include('defarticulos/insertDefArticulo')


  </div>
</section>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script type="text/javascript">

</script>
@stop