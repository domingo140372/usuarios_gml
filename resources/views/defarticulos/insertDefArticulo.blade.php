<div class="modal fade" tabindex="-1" role="dialog" id="addDefArticuloModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Nuevo Articulo</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form method="post" action="{{route('defarticulos.store')}}">
                @csrf
                @method('POST')                
                
                <div class="form-group">
                <label for="idArticulo">Articulo</label>
                      <select class="form-select form-select-lg" id="idArticulo" name="idArticulo">
                          <option selected>Selecione un Articulo</option>
                          @foreach($articulos as $articulo)
                          <option value="{{ $articulo->id }}">{{ $articulo->nombre_articulo }}</option>
                          @endforeach                      
                      </select>
                </div>
                <div class="form-group">
                <label for="idUnidad">Unidad de Medidad</label>
                      <select class="form-select form-select-lg" id="idUnidad" name="idUnidad">
                          <option selected>Selecione Unidad de Medidad</option>
                          @foreach($unidades as $unidades)
                          <option value="{{ $unidades->id }}">{{ $unidades->unidad }}</option>
                          @endforeach                      
                      </select>
                </div>
                <div class="form-group">
                    <label for="campo">Campo</label>
                    <input type="text" class="form-control" id="campo" name="campo"  aria-describedby="campo" placeholder="campo">
                    <small id="campo" class="form-text text-muted">campo.</small>
                </div> 
                <div class="form-group">
                    <label for="nota">Nota</label>
                    <input type="text" class="form-control" id="nota" name="nota"  aria-describedby="nota" placeholder="nota">
                    <small id="nota" class="form-text text-muted">nota.</small>
                </div>
                                
                <div class="form-group">
                    <button type="submit" id ="btnInsertarDefArticulo" class="btn btn-info">Guardar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>        
        </div>
    </div>
</div>