<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\RoleController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return redirect()->to('login');
});

Route::get('/register', function () {
    return redirect()->to('login');
});

Route::get('/home', function () {
    return redirect()->to('/admin/home');
});

Route::group([ 'prefix' => 'admin' ,'middleware' => 'auth' ], function () {

    Route::get('/home', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('home');
    Route::resource('roles', RoleController::class,);

    // Users CRUD
    Route::get('users/',
        [
            App\Http\Controllers\Admin\UserController::class,
            'index'
        ])
    ->name('users.index');  
    
    Route::post('users/crear/',
        [
            App\Http\Controllers\Admin\UserController::class,
            'store'
        ])
    ->name('users.store');    
    
    Route::put('users/actualizar/{id}',
        [
            App\Http\Controllers\Admin\UserController::class,
            'update'
        ])
    ->name('users.update');    
    
    Route::delete('users/borrar/{id}',
    [
        App\Http\Controllers\Admin\UserController::class,
        'destroy'
    ])
    ->name('users.destroy');
    
    Route::post('users/importar',
    [
        App\Http\Controllers\Admin\UserController::class,
        'importarUsuarios'
    ])
    ->name('users.import');
        

    

    // motos CRUD
    Route::get('motos/',
    [
        App\Http\Controllers\Admin\MotosController::class,
        'index'
    ])
    ->name('motos.index');

    Route::get('motos/insertar/',
    [
        App\Http\Controllers\Admin\MotosController::class,
        'create'
    ])
    ->name('motos.create');

    Route::get('motos/editar/{id}',
    [
        App\Http\Controllers\Admin\MotosController::class,
        'edit'
    ])
    ->name('motos.edit');

    Route::post('motos/',
    [
        App\Http\Controllers\Admin\MotosController::class,
        'store'
    ])
    ->name('motos.store');

    Route::put('motos/actualizar/{id}',
    [
        App\Http\Controllers\Admin\MotosController::class,
        'update'
    ])
    ->name('motos.update');

    Route::post('motos/borrado/{id}',
    [
        App\Http\Controllers\Admin\MotosController::class,
        'borrado'
    ])
    ->name('motos.borrado');

    Route::delete('motos/borrar/{id}',
    [
        App\Http\Controllers\Admin\MotosController::class,
        'destroy'
    ])
    ->name('motos.destroy');
    
    /**
     * Rutas de automoviñes
     */
    Route::get('autos/',
    [
        App\Http\Controllers\Admin\AutomovilesController::class,
        'index'
    ])
    ->name('autos.index');

    Route::get('autos/insertar/',
    [
        App\Http\Controllers\Admin\AutomovilesController::class,
        'create'
    ])
    ->name('autos.create');

    Route::get('autos/editar/{id}',
    [
        App\Http\Controllers\Admin\AutomovilesController::class,
        'edit'
    ])
    ->name('autos.edit');

    Route::post('autos/',
    [
        App\Http\Controllers\Admin\AutomovilesController::class,
        'store'
    ])
    ->name('autos.store');

    Route::put('autos/actualizar/{id}',
    [
        App\Http\Controllers\Admin\AutomovilesController::class,
        'update'
    ])
    ->name('autos.update');

    Route::delete('autos/borrar/{id}',
    [
        App\Http\Controllers\Admin\AutomovilesController::class,
        'destroy'
    ])
    ->name('autos.destroy');

    Route::post('autos/borrado/{id}',
    [
        App\Http\Controllers\Admin\AutomovilesController::class,
        'borrado'
    ])
    ->name('autos.borrado');


    
    /**
     * Rutas de genericos
     */
    Route::get('genericos/',
    [
        App\Http\Controllers\Admin\ArticulosGenericosController::class,
        'index'
    ])
    ->name('genericos.index');

    Route::get('genericos/insertar/',
    [
        App\Http\Controllers\Admin\ArticulosGenericosController::class,
        'create'
    ])
    ->name('genericos.create');

    Route::get('genericos/editar/{id}',
    [
        App\Http\Controllers\Admin\ArticulosGenericosController::class,
        'edit'
    ])
    ->name('genericos.edit');

    Route::post('genericos/',
    [
        App\Http\Controllers\Admin\ArticulosGenericosController::class,
        'store'
    ])
    ->name('genericos.store');
    
    Route::post('genericos/borrado/{id}',
    [
        App\Http\Controllers\Admin\ArticulosGenericosController::class,
        'borrado'
    ])
    ->name('genericos.borrado');

    Route::put('genericos/actualizar/{id}',
    [
        App\Http\Controllers\Admin\ArticulosGenericosController::class,
        'update'
    ])
    ->name('genericos.update');  

    
    Route::delete('genericos/borrar/{id}',
    [
        App\Http\Controllers\Admin\ArticulosGenericosController::class,
        'destroy'
    ])
    ->name('genericos.destroy');




     // renglones CRUD
     Route::get('renglones/',
     [
         App\Http\Controllers\Admin\RenglonesController::class,
         'index'
     ])
     ->name('renglones.index');   
     
     Route::post('renglones/crear/',
     [
         App\Http\Controllers\Admin\RenglonesController::class,
         'store'
     ])
     ->name('renglones.store');

     Route::put('renglones/actualizar/{id}',
    [
        App\Http\Controllers\Admin\RenglonesController::class,
        'update'
    ])
    ->name('renglones.update');

    Route::delete('renglones/borrar/{id}',
    [
        App\Http\Controllers\Admin\RenglonesController::class,
        'destroy'
    ])
    ->name('renglones.destroy');


    // Articulos CRUD
    Route::get('articulos/',
    [
        App\Http\Controllers\Admin\ArticulosController::class,
        'index'
    ])
    ->name('articulos.index');   
    
    Route::post('articulos/crear/',
    [
        App\Http\Controllers\Admin\ArticulosController::class,
        'store'
    ])
    ->name('articulos.store');

    Route::put('articulos/actualizar/{id}',
   [
       App\Http\Controllers\Admin\ArticulosController::class,
       'update'
   ])
   ->name('articulos.update');

   Route::delete('articulos/borrar/{id}',
   [
       App\Http\Controllers\Admin\ArticulosController::class,
       'destroy'
   ])
   ->name('articulos.destroy');


   // Definicion de Articulos CRUD
   Route::get('fotoMoto/',
   [
       App\Http\Controllers\Admin\MotosController::class,
       'fotoMoto'
   ])
   ->name('fotoMoto.get');   
   
   Route::post('fotoMoto/crear/',
   [
       App\Http\Controllers\Admin\MotosController::class,
       'agregarFotos'
   ])
   ->name('fotoMoto.store');

   Route::put('fotoMoto/actualizar/{id}',
  [
      App\Http\Controllers\Admin\MotosController::class,
      'update'
  ])
  ->name('fotoMoto.update');

  Route::delete('fotoMoto/borrar/',
  [
      App\Http\Controllers\Admin\MotosController::class,
      'eliminarFoto'
  ])
  ->name('fotoMoto.destroy');



  /**
   * Fotos de automovies
   */
  Route::get('fotoAuto/',
  [
      App\Http\Controllers\Admin\AutomovilesController::class,
      'fotoAuto'
  ])
  ->name('fotoAuto.get');   
  
  Route::post('fotoAuto/crear/',
  [
      App\Http\Controllers\Admin\AutomovilesController::class,
      'agregarFotos'
  ])
  ->name('fotoAuto.store');

  Route::put('fotoAuto/actualizar/{id}',
 [
     App\Http\Controllers\Admin\AutomovilesController::class,
     'update'
 ])
 ->name('fotoAuto.update');

 Route::delete('fotoAuto/borrar/',
 [
     App\Http\Controllers\Admin\AutomovilesController::class,
     'eliminarFoto'
 ])
 ->name('fotoAuto.destroy');


 /**
   * Fotos de Genericos
   */
  Route::get('fotoGenerico/',
  [
      App\Http\Controllers\Admin\ArticulosGenericosController::class,
      'fotoGenerico'
  ])
  ->name('fotoGenerico.get');   
  
  Route::post('fotoGenerico/crear/',
  [
      App\Http\Controllers\Admin\ArticulosGenericosController::class,
      'agregarFotos'
  ])
  ->name('fotoGenerico.store');

  Route::put('fotoGenerico/actualizar/{id}',
 [
     App\Http\Controllers\Admin\ArticulosGenericosController::class,
     'update'
 ])
 ->name('fotoGenerico.update');

 Route::delete('fotoGenerico/borrar/',
 [
     App\Http\Controllers\Admin\ArticulosGenericosController::class,
     'eliminarFoto'
 ])
 ->name('fotoGenerico.destroy');

    
    //logout
    Route::get('logout', function(){
        Auth::logout();
        return Redirect::to('login');
    });

    

   

});