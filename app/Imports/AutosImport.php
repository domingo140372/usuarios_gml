<?php

namespace App\Imports;

use App\Models\Automoviles;
use Maatwebsite\Excel\Concerns\{ToModel, WithHeadingRow};

class AutosImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Automoviles([
            //
        ]);
    }
}
