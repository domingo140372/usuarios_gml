<?php

namespace App\Imports;

use App\Models\User;

use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\{ToModel, WithHeadingRow};

class UserImport implements ToModel, WithHeadingRow
{
    /**
     * EXCEL COLUMNS FORMAT
     * NAME | EMAIL
     * @param array $row
     */
    public function model(array $row)
    {
       
        $userExists = User::whereEmail($row['email'])->first();

        if ( ! $userExists) {
            $user = User::create(
                [
                    'name'     => $row['name'],
                    'email'    => $row['email'],
                    'password' => Hash::make($row['password']),
                ]
            );
            return $user;
        }
    }

    public function startRow(): int {
        return 2;
    }
}
