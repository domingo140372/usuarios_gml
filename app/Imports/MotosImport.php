<?php

namespace App\Imports;

use App\Models\Motos;
use Maatwebsite\Excel\Concerns\{ToModel, WithHeadingRow};

class MotosImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $moto = new Motos([
            'id_articulo' => $row['idArticulo'],
            'modelo'  => $row['modelo'],
            'marca' => $row[' marca'],
            'serial_chasis' => $row[' serial_chasis'],
            'serial_motor' => $row['serial_motor'],
            'cilindrada' => $row['cilindrada'],
            'destinatario' => $row['destinatario'],
            'fecha_salida' => $row['fecha_salida'],
            'fecha_salida' => $row['fecha_entrada'],
            'color' => $row['color'],
            'placa'  => $row['placa'],
            'rotulada' => $row['rotulada'] ,
            'operativa' => $row['operativa'],
            'entregada' => $row['entregada'],
            'facturacion' => $row['facturacion'],
            'observacion' => $row['observacion'],
        ]);
        return $moto;
    }
}
