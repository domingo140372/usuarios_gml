<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class FotosAutomiviles extends Model
{
    use HasFactory;
    protected $fillable = [
        "ruta" ,
        "id_auto",
    ];

    /**
    * Get motos.
    */
    public function autos(): HasOne
    {
        return $this->hasOne(Automoviles::class,'id','id_auto');
    
    }

}
