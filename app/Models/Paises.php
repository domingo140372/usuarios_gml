<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Paises extends Model
{
    use HasFactory;
    protected $fillable = [
        'nombre_pais',
        'latitud',
        'longitud',
    ];


    public function usuario(): BelongsTo
    {
        return $this->belongsTo(User::class,'id_pais', 'id');
    }
}
