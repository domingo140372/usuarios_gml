<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;


class FotosMotos extends Model
{
    use HasFactory;
    protected $fillable = [
        "ruta" ,
        "id_moto",
    ];

    /**
    * Get motos.
    */
    public function motos(): HasOne
    {
        return $this->hasOne(Motos::class,'id','id_moto');
    
    }

}
