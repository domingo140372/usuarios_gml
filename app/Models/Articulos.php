<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;


class Articulos extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nombre_articulo',
        'codigo',
        'id_renglon'
    ];


    /**
     * Get motos por articulo.
     */
    public function motos(): HasMany
    {
        return $this->hasMany(Motos::class, 'id_articulo', 'id');
    }

    /**
     * Get automoviles por articulo.
     */
    public function autos(): HasMany
    {
        return $this->hasMany(Automoviles::class, 'id_articulo', 'id');
    }

    /**
     * Get genericos por articulo.
     */
    public function genericos(): HasMany
    {
        return $this->hasMany(ArticulosGenericos::class, 'id_articulo', 'id');
    }

     /**
     * Get renglones.
     */
    public function renglones(): HasOne
    {
        return $this->hasOne(Renglones::class,'id','id_renglon');
    
    }
}
