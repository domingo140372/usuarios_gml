<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Automoviles extends Model
{
    use HasFactory;
    use HasFactory;
     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [ 
                'id_articulo',
                'modelo' ,
                'marca',
                'serial_chasis',
                'serial_motor',
                'fecha_salida',
                'fecha_entrada',
                'color',
                'destinatario',
                'placa' ,
                'rotulada',
                'operativa',
                'entregada',
                'borrado',
                'facturacion',
                'observacion',
            ];
    
    
     /**
    * Get articulos.
    */
    public function articulos(): HasOne
    {
        return $this->hasOne(Articulos::class,'id','id_articulo');
    
    }

    /**
    * Get fotos de automoviles.
    */
    public function fotosAuto(): HasMany
    {
        return $this->hasMany(FotosAutomiviles::class, 'id_auto', 'id');
    }
}
