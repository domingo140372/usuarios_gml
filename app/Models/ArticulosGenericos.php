<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ArticulosGenericos extends Model
{
    use HasFactory;
     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
            'id_articulo',
            'id_unidad',
            'modelo' ,
            'marca',
            'codigo' ,
            'cantidad' ,
            'fecha_salida' ,
            'fecha_ingreso' ,
            'estado',
            'entregada',
            'borrado' ,
            'descripcion',
            'observacion',
    ];

    
    
    /**
    * Get articulos.
    */
    public function articulos(): HasOne
    {
        return $this->hasOne(Articulos::class,'id','id_articulo');
    
    }

    /**
    * Get articulos.
    */
    public function unidades(): HasOne
    {
        return $this->hasOne(UnidadesMedida::class,'id','id_unidad');
    
    }

    /**
    * Get fotos de automoviles.
    */
    public function fotosGenericos(): HasMany
    {
        return $this->hasMany(FotosGenericos::class, 'id_generico', 'id');
    }


}
