<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class UnidadesMedida extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'unidad','descripcion',
    ];

    public function usuario(): HasMany
    {
        return $this->hasMany(ArticulosGenericos::class,'id_unidad', 'id');
    }
}
