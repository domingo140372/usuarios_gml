<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class FotosGenericos extends Model
{
    use HasFactory;
    protected $fillable = [
        "ruta" ,
        "id_generico",
    ];

    /**
    * Get motos.
    */
    public function genericos(): HasOne
    {
        return $this->hasOne(ArticulosGenericos::class,'id','id_generico');
    
    }

}
