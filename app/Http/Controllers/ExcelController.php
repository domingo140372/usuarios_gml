<?php

namespace App\Http\Controllers;

use App\Imports\UserImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    //
   

    public function userImport(Request $request)
    {
        // Validar que se ha subido un archivo
        $request->validate([
            'file' => 'required|mimes:xlsx,csv'
        ]);

        // Importar el archivo utilizando la clase UsersImport
        Excel::import(new UserImport, $request->file('file'));

        // Redirigir con un mensaje de éxito
        return redirect('/')->with('success', 'Datos importados con éxito!');
    }

}
