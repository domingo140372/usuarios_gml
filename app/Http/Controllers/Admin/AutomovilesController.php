<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Automoviles;
use App\Models\FotosAutomiviles;
use App\Models\Articulos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Imports\AutosImport;
use Maatwebsite\Excel\Facades\Excel;

class AutomovilesController extends Controller
{   
    private $nombreCarpetaFotos = "fotos_autos";
    private $nombreFoto404 = "error_404.jpeg";
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $autos = Automoviles::orderBy('id')->with('fotosAuto')->paginate(10);
        $articulos = Articulos::all()->where('id_renglon', '=', 1);
        return view('autos.index', ['autos'=>$autos,
                                        'articulos'=>$articulos]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $articulos = Articulos::all()->where('id_renglon', '=', 1);
        return view('autos.insertar',['articulos'=>$articulos]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $auto = new Automoviles;
        $auto->id_articulo = $request->id_articulo;
        $auto->modelo  = $request->modelo;
        $auto->marca = $request-> marca;
        $auto->serial_chasis = $request-> serial_chasis;
        $auto->serial_motor = $request->serial_motor;
        $auto->fecha_salida = $request->fecha_salida;
        $auto->fecha_entrada = $request->fecha_entrada;
        $auto->color = $request->color;
        $auto->placa  = $request->placa;
        $auto->destinatario  = $request->destinatario;
        $auto->rotulada = $request->rotulada ? '1' : '0';
        $auto->operativa = $request->operativa ? '1' : '0';
        $auto->entregada = $request->entregada ? '1' : '0';      
        $auto->borrado = 0;      
        $auto->facturacion = $request->facturacion;
        $auto->observacion = $request->observacion;

        $auto->save();
        return $this->index();
    }

    

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //
         /**aqui voy a llamar el formulario de editar */
         $auto = Automoviles::where('id', '=', $id)->first();       
         $articulos = Articulos::all()->where('id_renglon', '=', 1);        
         return view('autos.actualizar',['auto'=>$auto,'articulos'=>$articulos, ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        //
        $auto = Automoviles::findOrFail($id);
        $auto->fill([
            'id_articulo' => $request->id_articulo,
            'modelo'  => $request->modelo,
            'marca' => $request-> marca,
            'serial_chasis' => $request-> serial_chasis,
            'serial_motor' => $request->serial_motor,
            'fecha_salida' => $request->fecha_salida,
            'fecha_entrada' => $request->fecha_entrada,
            'color' => $request->color,
            'destinatario' => $request->destinatario,
            'placa'  => $request->placa,
            'rotulada' => $request->rotulada ? '1' : '0',
            'operativa' => $request->operativa ? '1' : '0',
            'entregada' => $request->entregada ? '1' : '0',
            'borrado' => 0,
            'facturacion' => $request->facturacion,
            'observacion' => $request->observacion,
        ]);
        $auto->save();
        return $this->index();
    }

    /**
     * Remove the specified resource with id
     */
    public function borrado($id)
    {
        //
        $auto = Automoviles::findOrFail($id);
        $auto->where('id', $id)
      		->update(['borrado' => 1 ]);
        return $this->index();
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Automoviles $automoviles)
    {
        //
    }

     /*******************************************
     * Agregar fotos relacionadas a las motos  *
     *******************************************/
    private function obtenerRutaSegura($rutaInsegura)
    {
        # Verifica si la ruta que se quiere leer realmente pertenece a un artículo, en caso de que sí, regresa la
        #misma ruta pero tomada de la base de datos
        $posibleRegistro = FotosAutomiviles::where("ruta", "=", $rutaInsegura)->first();
        if ($posibleRegistro === null) return $this->nombreFoto404;
        return $posibleRegistro->ruta;
    }

    public function fotoAuto(Request $request)
    {
        $nombreFoto = $request->nombre;
        $rutaDeImagen = storage_path("app/" . $this->nombreCarpetaFotos . "/" . $this->obtenerRutaSegura($nombreFoto));
        return response()->file($rutaDeImagen);
    }

    public function descargarFoto(Request $request)
    {
        $nombreFoto = $request->nombre;
        $rutaDeImagen = storage_path("app/" . $this->nombreCarpetaFotos . "/" . $this->obtenerRutaSegura($nombreFoto));
        return response()->download($rutaDeImagen);
    }

    public function agregarFotos(Request $request)
    {
        $imageUrls = [];

        if ($request->hasFile('fotos')) {
            foreach ($request->file('fotos') as $image) {
                $filename = $image->store($this->nombreCarpetaFotos);
                $productImage = new FotosAutomiviles();
                $productImage->id_auto = $request->id_auto;
                $productImage->ruta = str_replace("$this->nombreCarpetaFotos/", "", $filename);;
                $productImage->save();

                $imageUrls[] = $filename;
            }
        }
        return back()
            ->with("message", "Foto(s) agregada(s) con éxito")
            ->with("session", "success");
    }

    public function administrarFotos(Request $peticion)
    {
        /*$idArticulo = $peticion->id;
        return view("articulos.fotos", ["articulo" => Articulo::with(["area", "fotos"])->findOrFail($idArticulo)]);
        */
    }

    public function eliminarFoto(Request $request)
    {
        $foto_archivo = $request->ruta;
        $foto_bd = FotosAutomiviles::find($request->id);
        $nombreFoto = $this->obtenerRutaSegura($foto_archivo);
        if ($nombreFoto === $this->nombreFoto404) return [];
        Storage::disk("local")->delete("fotos/$nombreFoto");
        $foto_bd->delete();
        return $this->index();
    }

    

}
