<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\ArticulosGenericos;
use App\Models\UnidadesMedida;
use App\Models\FotosGenericos;
use App\Models\Articulos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Imports\GenericosImport;
use Maatwebsite\Excel\Facades\Excel;

class ArticulosGenericosController extends Controller
{
    private $nombreCarpetaFotos = "fotos_genericos";
    private $nombreFoto404 = "error_404.jpeg";
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $genericos = ArticulosGenericos::orderBy('id')->where('borrado','=',0)->with('fotosGenericos')->paginate(10);
        $unidades = UnidadesMedida::all();
        $articulos = Articulos::all()->whereIn('id_renglon',array(3,4) );
        return view('genericos.index', ['genericos'=>$genericos, 'unidades'=>$unidades,
                                        'articulos'=>$articulos]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $unidades = UnidadesMedida::all();
        $articulos = Articulos::whereIn('id_renglon',array (3, 4))->get();
        return view('genericos.insertar',['articulos'=>$articulos, 'unidades'=>$unidades]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $articulo = new ArticulosGenericos();
        $articulo->id_articulo = $request->idArticulo;
        $articulo->id_unidad = $request->idUnidad;
        $articulo->modelo  = $request->modelo;
        $articulo->marca = $request-> marca;
        $articulo->codigo = $request->codigo;
        $articulo->cantidad = $request->cantidad;
        $articulo->fecha_salida = $request->fecha_salida;
        $articulo->fecha_ingreso = $request->fecha_ingreso;
        $articulo->estado = $request->estado ;
        $articulo->entregada = $request->entregada ? '1' : '0';    
        $articulo->borrado = 0;    
        $articulo->descripcion = $request->descripcion;
        $articulo->observacion = $request->observacion;

        $articulo->save();
        return $this->index();
    }

    
    public function edit($id)
    {
        /**aqui voy a llamar el formulario de editar */
        $generico = ArticulosGenericos::where('id', '=', $id)->first();
        $unidades = UnidadesMedida::all();
        $articulos = Articulos::whereIn('id_renglon',array (3, 4))->get();
        return view('genericos.actualizar',['generico'=>$generico,'articulos'=>$articulos, 'unidades'=>$unidades]);
    }
   
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,  $id)
    {
        //
        //se actualiza los datos del Usuario
        $generico = ArticulosGenericos::findOrFail($id);
        $generico->fill([
            'id_articulo' => $request->id_articulo,
            'id_unidad' => $request->id_unidad,
            'modelo'  => $request->modelo,
            'marca' => $request->marca,
            'codigo' => $request->codigo,
            'cantidad' => $request->cantidad,
            'fecha_salida' => $request->fecha_salida,
            'fecha_ingreso' => $request->fecha_ingreso,
            'estado' => $request->estado,
            'entregada' => $request->entregada ? '1' : '0',
            'borrado' => 0,
            'descripcion' => $request->descripcion,
            'observacion' => $request->observacion,
        ]);
        $generico->save();
        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function borrado(Request $request,  $id)
    {
        $generico = ArticulosGenericos::findOrFail($id);
        $generico->where('id', $id)
      		->update(['borrado' => 1 ]);
        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //
        $generico = ArticulosGenericos::findOrFail($id);
        $generico->fill([
            'borrado' => 1,
        ]);
        $generico->save();
        return $this->index();
    }




     /*******************************************
     * Agregar fotos relacionadas a las motos  *
     *******************************************/
    private function obtenerRutaSegura($rutaInsegura)
    {
        # Verifica si la ruta que se quiere leer realmente pertenece a un artículo, en caso de que sí, regresa la
        #misma ruta pero tomada de la base de datos
        $posibleRegistro = FotosGenericos::where("ruta", "=", $rutaInsegura)->first();
        if ($posibleRegistro === null) return $this->nombreFoto404;
        return $posibleRegistro->ruta;
    }

    public function fotoGenerico(Request $request)
    {
        $nombreFoto = $request->nombre;
        $rutaDeImagen = storage_path("app/" . $this->nombreCarpetaFotos . "/" . $this->obtenerRutaSegura($nombreFoto));
        return response()->file($rutaDeImagen);
    }

    public function descargarFoto(Request $request)
    {
        $nombreFoto = $request->nombre;
        $rutaDeImagen = storage_path("app/" . $this->nombreCarpetaFotos . "/" . $this->obtenerRutaSegura($nombreFoto));
        return response()->download($rutaDeImagen);
    }

    public function agregarFotos(Request $request)
    {
        $imageUrls = [];

        if ($request->hasFile('fotos')) {
            foreach ($request->file('fotos') as $image) {
                $filename = $image->store($this->nombreCarpetaFotos);
                $productImage = new FotosGenericos();
                $productImage->id_generico = $request->id_generico;
                $productImage->ruta = str_replace("$this->nombreCarpetaFotos/", "", $filename);;
                $productImage->save();

                $imageUrls[] = $filename;
            }
        }
        return back()
            ->with("message", "Foto(s) agregada(s) con éxito")
            ->with("session", "success");
    }

    public function administrarFotos(Request $peticion)
    {
        /*$idArticulo = $peticion->id;
        return view("articulos.fotos", ["articulo" => Articulo::with(["area", "fotos"])->findOrFail($idArticulo)]);
        */
    }

    public function eliminarFoto(Request $request)
    {
        $foto_archivo = $request->ruta;
        $foto_bd = FotosGenericos::find($request->id);
        $nombreFoto = $this->obtenerRutaSegura($foto_archivo);
        if ($nombreFoto === $this->nombreFoto404) return [];
        Storage::disk("local")->delete("fotos/$nombreFoto");
        $foto_bd->delete();
        return $this->index();
    }

    public function importarGenericos(Request $request) 
    {
        // Validar que se ha subido un archivo
        $request->validate([
            'file' => 'required|mimes:xlsx,csv'
        ]);

        // Importar el archivo utilizando la clase UsersImport
        $import = Excel::import(new GenericosImport, $request->file('file'));

        // Redirigir con un mensaje de éxito       
        return back()->with('success', 'Datos importados con éxito!');
       
    }

}
