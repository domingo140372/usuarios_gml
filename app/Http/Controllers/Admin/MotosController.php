<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Motos;
use App\Models\FotosMotos;
use App\Models\Articulos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Imports\MotosImport;
use Maatwebsite\Excel\Facades\Excel;

class MotosController extends Controller
{
    
    private $nombreCarpetaFotos = "fotos_motos";
    private $nombreFoto404 = "error_404.jpeg";

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //

        $motos = Motos::orderBy('id')->where('borrado', '=', 0)->with('fotosMoto')->paginate(10);
        $articulos = Articulos::all()->where('id_renglon', '=', 2);
        return view('motos.index', ['motos'=>$motos,
                                        'articulos'=>$articulos]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $articulos = Articulos::all()->where('id_renglon', '=', 2);
        return view('motos.insertar',['articulos'=>$articulos]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $moto = new Motos;
        $moto->id_articulo = $request->idArticulo;
        $moto->modelo  = $request->modelo;
        $moto->marca = $request-> marca;
        $moto->serial_chasis = $request-> serial_chasis;
        $moto->serial_motor = $request->serial_motor;
        $moto->cilindrada = $request->cilindrada;
        $moto->fecha_salida = $request->fecha_salida;
        $moto->fecha_entrada = $request->fecha_entrada;
        $moto->color = $request->color;
        $moto->destinatario = $request->destinatario;
        $moto->placa  = $request->placa;
        $moto->rotulada = $request->rotulada ? '1' : '0';
        $moto->operativa = $request->operativa ? '1' : '0';
        $moto->entregada = $request->entregada ? '1' : '0';       
        $moto->borrado = 0;       
        $moto->facturacion = $request->facturacion;
        $moto->observacion = $request->observacion;

        $moto->save();
        return $this->index();
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        /**aqui voy a llamar el formulario de editar */
        $moto = Motos::where('id', '=', $id)->first();       
        $articulos = Articulos::all()->where('id_renglon', '=', 2);        
        return view('motos.actualizar',['moto'=>$moto,'articulos'=>$articulos, ]);
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        //
        $moto = Motos::findOrFail($id);
        $moto->fill([
            'id_articulo' => $request->idArticulo,
            'modelo'  >= $request->modelo,
            'marca' => $request-> marca,
            'serial_chasis' => $request-> serial_chasis,
            'serial_motor' => $request->serial_motor,
            'cilindrada' => $request->cilindrada,
            'fecha_salida' => $request->fecha_salida,
            'fecha_entrada' => $request->fecha_entrada,
            'color' => $request->color,
            'destinatario' => $request->destinatario,
            'placa'  => $request->placa,
            'rotulada' => $request->rotulada ? '1' : '0',
            'operativa' => $request->operativa ? '1' : '0',
            'entregada' => $request->entregada ? '1' : '0',
            'borrado' => 0,
            'facturacion' => $request->facturacion,
            'observacion' => $request->observacion,

        ]);
        $moto->save();
        return $this->index();
    }

    
    /**
     * Remove the specified resource with id
     */
    public function borrado($id)
    {
        //
        $moto = Motos::findOrFail($id);
        $moto->where('id', $id)
      		->update(['borrado' => 1 ]);
        return $this->index();
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Motos $motos)
    {
        //
    }



    /*******************************************
     * Agregar fotos relacionadas a las motos  *
     *******************************************/
    private function obtenerRutaSegura($rutaInsegura)
    {
        # Verifica si la ruta que se quiere leer realmente pertenece a un artículo, en caso de que sí, regresa la
        #misma ruta pero tomada de la base de datos
        $posibleRegistro = FotosMotos::where("ruta", "=", $rutaInsegura)->first();
        if ($posibleRegistro === null) return $this->nombreFoto404;
        return $posibleRegistro->ruta;
    }

    public function fotoMoto(Request $request)
    {
        $nombreFoto = $request->nombre;
        $rutaDeImagen = storage_path("app/" . $this->nombreCarpetaFotos . "/" . $this->obtenerRutaSegura($nombreFoto));
        return response()->file($rutaDeImagen);
    }

    public function descargarFoto(Request $request)
    {
        $nombreFoto = $request->nombre;
        $rutaDeImagen = storage_path("app/" . $this->nombreCarpetaFotos . "/" . $this->obtenerRutaSegura($nombreFoto));
        return response()->download($rutaDeImagen);
    }

    public function agregarFotos(Request $request)
    {
        $imageUrls = [];

        if ($request->hasFile('fotos')) {
            foreach ($request->file('fotos') as $image) {
                $filename = $image->store($this->nombreCarpetaFotos);
                $productImage = new FotosMotos();
                $productImage->id_moto = $request->id_moto;
                $productImage->ruta = str_replace("$this->nombreCarpetaFotos/", "", $filename);;
                $productImage->save();

                $imageUrls[] = $filename;
            }
        }
        return back()
            ->with("message", "Foto(s) agregada(s) con éxito")
            ->with("session", "success");
    }

    public function administrarFotos(Request $peticion)
    {
        /*$idArticulo = $peticion->id;
        return view("articulos.fotos", ["articulo" => Articulo::with(["area", "fotos"])->findOrFail($idArticulo)]);
        */
    }

    public function eliminarFoto(Request $request)
    {
        $foto_archivo = $request->ruta;
        $foto_bd = FotosMotos::find($request->id);
        $nombreFoto = $this->obtenerRutaSegura($foto_archivo);
        if ($nombreFoto === $this->nombreFoto404) return [];
        Storage::disk("local")->delete("fotos/$nombreFoto");
        $foto_bd->delete();
        return $this->index();
    }



}
