<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use App\Imports\UserImport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {          
        $usuarios = User::all();
        $roles = Role::all();         
        
        return view('users.index', ['usuarios'=>$usuarios, 
                                     'roles'=>$roles]);
        
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));
    }

    
    /**
     * Store un nuevo registro en la BD.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        
        /*$request->validate([
            'name' => [
                'required',
                'string',
                'regex:/^[a-zA-Z]+$/',
                'max:100',
                'min:5'
            ],
            'password' => [
                'required', 
                'min:4'
            ],
            'email' => [
                'required',
                'email',
                'unique:users',
                'max:150'
            ],
            
        ]);*/

        $usuario = new User([
            'name' => $request->nombre,
            'email' => $request->correo,
            'password' => Hash::make($request->clave),            
        ]);
        $usuario->save(); 
        $role = Role::where('id', $request->idRol)->first(); 
        $usuario->assignRole($role);
        return $this->index()->with('success','User created successfully');;

    }   

    
    /**
     * Se actualiza un usuario especifico en la BD.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //se actualiza los datos del Usuario
        $usuario = User::findOrFail($id);
        $usuario->fill([
            'name' => $request->nombre,
            'email' => $request->correo,
        ]);
        
        $usuario->save(); 
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $role = Role::where('id', $request->idRol)->first();      
        $usuario->assignRole($role);
        return $this->index();
        
    }

    /**
     * Eliminar un suario especifico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        /*
        * Se elimina un suario (en mi concepto este tipo de funciones no deberian existir)
        * la elimicaion de datos en una BD en produccion debe ser logica y no fisica
        * se debe crear un campo booleano True-False que permita esta accion
        */
        $usuario = User::find($id);        

        if (!$usuario) {
            return response()->json(['error' => 'El usuario no existe'], 404);
        }  
        $usuario->delete();       
        return $this->index();
    }

    public function importarUsuarios(Request $request) 
    {
        // Validar que se ha subido un archivo
        $request->validate([
            'file' => 'required|mimes:xlsx,csv'
        ]);

        // Importar el archivo utilizando la clase UsersImport
        $import = Excel::import(new UserImport, $request->file('file'));

        // Redirigir con un mensaje de éxito       
        return back()->with('success', 'Datos importados con éxito!');
       
    }
}
