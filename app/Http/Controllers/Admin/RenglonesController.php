<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Renglones;
use App\Models\Articulos;
use Illuminate\Http\Request;

class RenglonesController extends Controller
{
    //
    /**
     * Muestra la lista de Renglones.
     */
    public function index()
    {
        //
        $renglones = Renglones::orderBy('id')->paginate(10);
        return view('renglones.index', ['renglones'=>$renglones]);
    }

    /**
     * Se guarda un nuevo renglon en la BD.
     */
    public function store(Request $request)
    {
        $renglon = new Renglones;
        $renglon->nombre = $request->nombre;
        $renglon->descripcion = $request->descripcion;        

        $renglon->save();
        return $this->index();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        //
        $renglon = Renglones::findOrFail($id);
        
        $renglon->fill([
            'nombre' => $request->nombre,
            'descripcion' => $request->descripcion,
        ]);
        $renglon->save();
        return $this->index();
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //
        $renglon = Renglones::find($id);
        $renglon->articulo()->delete();
        $renglon->delete();
        return $this->index();    
    }


}
