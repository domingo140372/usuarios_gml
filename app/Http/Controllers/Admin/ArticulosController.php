<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Articulos;
use App\Models\Renglones;
use Illuminate\Http\Request;

class ArticulosController extends Controller
{
    /**
     * Muestra la lista de Articuloss.
     */
    public function index()
    {
        $articulos = Articulos::orderBy('id')->paginate(10);
        $renglones = Renglones::All();
        return view('articulos.index', ['articulos'=>$articulos,
                                            'renglones'=>$renglones]);
    }


    /**
     * Se guarda un nuevo renglon en la BD.
     */
    public function store(Request $request)
    {
        $articulo = new Articulos();
        $articulo->nombre_articulo = $request->nombre;
        $articulo->codigo = $request->codigo;
        $articulo->id_renglon = $request->idRenglon;
        $articulo->save();
        return $this->index();
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        //
       
        $articulo = Articulos::findOrFail($id);        
        $articulo->fill([
            'nombre_articulo' => $request->nombre,
            'codigo' => $request->codigo,
            'id_renglon' => $request->idRenglon,
        ]);
        $articulo->save();
        return $this->index();
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {   
        /*
            Primero debo borrar las definiciones del articulo si existen
        */
        $articulo = Articulos::find($id);
        //$articulo->def_articulos_renglones()->delete();
        $articulo->delete();        
        return $this->index();    
    }



}
