<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('articulos_genericos', function (Blueprint $table) {
            $table->id();                    
            $table->integer('id_articulo');        
            $table->integer('id_unidad');               
            $table->dateTime("fecha_ingreso");
            $table->dateTime("fecha_salida");
            $table->string("codigo", 255);
            $table->string("numero_serie", 255)->nullable();
            $table->string("descripcion", 255);
            $table->string("marca", 255);
            $table->string("modelo", 255);
            $table->float('cantidad'); 
            $table->boolean("entregada");
            $table->boolean("borrado");
            $table->string("estado", 255);
            $table->text("observacion")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('articulos_genericos');
    }
};
