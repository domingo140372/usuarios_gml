<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('motos', function (Blueprint $table) {
            $table->id();
            $table->integer('id_articulo');
            $table->string('modelo', 255);
            $table->string('marca', 255);
            $table->string('serial_chasis', 255);
            $table->string('serial_motor', 255);
            $table->string('cilindrada', 10);
            $table->string('destinatario', 255);
            $table->string('color');
            $table->dateTime('fecha_salida');
            $table->dateTime('fecha_entrada');
            $table->string('placa', 50);
            $table->boolean('rotulada');
            $table->boolean('operativa');
            $table->string('facturacion');
            $table->text('observacion');
            $table->boolean('entregada');
            $table->boolean('borrado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('motos');
    }
};

/*
modelo	
marca	
serial  de chasis
serial de motor	
cilindrada	
color 	 	
fecha de entrada	
fecha  de salida	
placa 	
rotulada	
operativa	
inoperativa 	
facturacion	
observacion 	
foto 
*/
