<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UnidadesMedida;


class UnidadesMedidaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $unidades =[
            'Metros',
            'Kilos',
            'Toneladas',
            'Unidad(es)',
            'Litros',
        ];
        foreach ($unidades as $unidad) {
            UnidadesMedida::create(['unidad' => $unidad, 'descripcion' =>'Unidad de medida']);
        }
    
    }
}