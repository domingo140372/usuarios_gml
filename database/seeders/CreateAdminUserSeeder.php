<?php

namespace Database\Seeders;
        
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
        
class CreateAdminUserSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $user = User::create([
        'name' => 'Domingo A Utrera',
        'email' => 'admin.test@gmail.com',        
        'password' => Hash::make('d0m1ng0'),       
        ]);
        
        $role = Role::create(['name' => 'Administrador']);
        
        $permissions = Permission::pluck('id','id')->all();
        
        $role->syncPermissions($permissions);
        
        $user->assignRole([$role->id]);
    }
}
      